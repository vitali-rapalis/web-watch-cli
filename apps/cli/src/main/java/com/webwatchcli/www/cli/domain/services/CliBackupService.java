package com.webwatchcli.www.cli.domain.services;


public interface CliBackupService {

    String backup(String filePath);
}

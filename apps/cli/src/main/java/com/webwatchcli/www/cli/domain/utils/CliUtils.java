package com.webwatchcli.www.cli.domain.utils;

import com.webwatchcli.www.cli.domain.models.CliWebWatchModel;
import lombok.experimental.UtilityClass;
import lombok.val;

import java.util.HashSet;
import java.util.Set;

@UtilityClass
public class CliUtils {

    public static final String PROVIDED_URLS_NOT_FOUND_MSG = "Provided urls not found";

    public static String printResult(Set<CliWebWatchModel> watchModelSet) {
        return printResult() +
                watchModelSet.stream()
                        .map(CliWebWatchModel::printResult)
                        .reduce("", (resultString, nextModel) -> resultString + nextModel + "\n");
    }

    public static void printResult(CliWebWatchModel model) {
        System.out.println(model.printResult());
    }

    public static String printResult() {
        return String.format("""
                RESULT:
                -------
                URL \t| STATUS \t| DATE (UTC)
                """);
    }

    public static Set<String> getUrlsToTest(Set<String> configuredUrls, Set<String> providedUrls) {
        val urlsToTest = new HashSet<>(configuredUrls);
        if (!providedUrls.isEmpty()) {
            urlsToTest.retainAll(providedUrls);
        }
        return urlsToTest;
    }

}

package com.webwatchcli.www.cli.domain.services;

public interface CliStartupService {

    void startUp();
}

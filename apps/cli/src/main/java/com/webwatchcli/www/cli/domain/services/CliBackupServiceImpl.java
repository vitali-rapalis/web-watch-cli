package com.webwatchcli.www.cli.domain.services;

import com.webwatchcli.www.cli.domain.props.CliProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.stereotype.Service;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

@Log4j2
@Service
@RequiredArgsConstructor
public class CliBackupServiceImpl implements CliBackupService {

    private final CliProperties cliProperties;
    private final CliAssertionService cliAssertionService;

    @Override
    public String backup(String filePath) {
        try {
            cliAssertionService.assertValidFileExtension(filePath);
            val targetBackupFile = Paths.get(filePath);
            Files.copy(cliProperties.getDataStoreFilePath(), targetBackupFile);
            return String.format("Info: Backup created path: %s", targetBackupFile.toAbsolutePath());
        } catch (Exception ex) {
            return String.format("Error: Backup failed, reason: %s", ex.getMessage());
        }
    }
}

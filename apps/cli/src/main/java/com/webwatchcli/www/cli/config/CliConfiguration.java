package com.webwatchcli.www.cli.config;

import com.webwatchcli.www.cli.domain.props.CliProperties;
import lombok.RequiredArgsConstructor;
import org.jline.utils.AttributedString;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ClientHttpConnector;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.shell.jline.PromptProvider;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;

@Configuration
@RequiredArgsConstructor
@EnableConfigurationProperties(value = CliProperties.class)
public class CliConfiguration {

    private final CliProperties cliProperties;

    @Bean
    public PromptProvider myPromptProvider() {
        return () -> new AttributedString(cliProperties.promptName());
    }

    @Bean
    public WebClient webClient() {
        HttpClient httpClient = HttpClient.create().followRedirect(true);
        ClientHttpConnector connector = new ReactorClientHttpConnector(httpClient);

        return WebClient.builder()
                .clientConnector(connector)
                .build();
    }
}

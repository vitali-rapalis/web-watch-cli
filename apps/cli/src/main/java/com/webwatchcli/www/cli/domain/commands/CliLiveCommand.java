package com.webwatchcli.www.cli.domain.commands;

import com.webwatchcli.www.cli.domain.services.CliLiveService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.hibernate.validator.constraints.URL;
import org.springframework.shell.standard.ShellCommandGroup;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.util.List;

import static com.webwatchcli.www.utils.textmessages.UtilsValidationMessages.Url.NOT_VALID_URL_MSG;

@Log4j2
@RequiredArgsConstructor
@ShellComponent("live")
@ShellCommandGroup("Cli Live Commands")
public class CliLiveCommand {

    private final CliLiveService cliLiveService;

    @ShellMethod(key = "live", value = "Queries the URLs of all configured website(s) or service(s) based on an interval")
    public void live(@ShellOption(value = {"--urls", "-u"},
            help = "URLs of configured website(s) or service(s) with comma separated, --urls https://google.com,https://amazon.com (optional argument)",
            defaultValue = ""
    ) List<@URL(message = NOT_VALID_URL_MSG) String> urls,
                     @ShellOption(value = {"--print", "-p"},
                             help = "Output the result of a website or service (optional argument)", defaultValue = "false") boolean print,
                     @ShellOption(value = {"--interval", "-i"},
                             help = "Configurable pooling interval with a sensible default: 5 seconds (optional argument)", defaultValue = "5") int interval,
                     @ShellOption(value = {"--timout", "-t"},
                                help = "Timout in seconds (optional argument)", defaultValue = "3") int timeout) {
        cliLiveService.live(urls, interval, print, timeout);
    }
}

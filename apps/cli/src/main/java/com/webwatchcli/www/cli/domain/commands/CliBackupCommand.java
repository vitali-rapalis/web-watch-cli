package com.webwatchcli.www.cli.domain.commands;

import com.webwatchcli.www.cli.domain.services.CliBackupService;
import com.webwatchcli.www.cli.domain.services.CliHistoryService;
import jakarta.validation.constraints.NotEmpty;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;
import org.springframework.shell.standard.ShellCommandGroup;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.util.Set;

import static com.webwatchcli.www.utils.textmessages.UtilsValidationMessages.Url.NOT_VALID_URL_MSG;


@Log4j2
@RequiredArgsConstructor
@ShellComponent("backup")
@ShellCommandGroup("Cli Backup Commands")
public class CliBackupCommand {

    private final CliBackupService cliBackupService;

    @ShellMethod(key = "backup", value = "Takes a file path argument and creates a backup of data in current data store")
    public String history(@ShellOption(value = {"--filePath", "-f"},
            help = "File path in which backup file will be stored, e.x /home/user/.web-watch-cli/backup-copy.txt"
    ) @NotEmpty String filePath) {
        return cliBackupService.backup(filePath);
    }
}

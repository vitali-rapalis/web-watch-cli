package com.webwatchcli.www.cli.domain.services;

public interface CliRestoreService {

    String restore(String filePath);
}

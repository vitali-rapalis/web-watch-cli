package com.webwatchcli.www.cli.domain.commands;

import com.webwatchcli.www.cli.domain.services.CliRestoreService;
import jakarta.validation.constraints.NotEmpty;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.shell.standard.ShellCommandGroup;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;


@Log4j2
@RequiredArgsConstructor
@ShellComponent("restore")
@ShellCommandGroup("Cli Restore Commands")
public class CliRestoreCommand {

    private final CliRestoreService cliRestoreService;

    @ShellMethod(key = "restore", value = "Takes a file path argument and restores the data in the file into current data store")
    public String history(@ShellOption(value = {"--filePath", "-f"},
            help = "Backup file path e.x /home/user/.web-watch-cli/backup-copy.txt"
    ) @NotEmpty String filePath) {
        return cliRestoreService.restore(filePath);
    }
}

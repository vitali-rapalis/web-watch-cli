package com.webwatchcli.www.cli.domain.services;

import com.webwatchcli.www.cli.domain.services.CliDataStoreService;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

@Service
@RequiredArgsConstructor
public class CliAssertionService {

    private final CliDataStoreService dataStoreService;

    public void assertFileIsValid(String filePath) throws IOException {
        val path = Paths.get(filePath);

        if (!Files.probeContentType(path).startsWith("text/")) {
            throw new IOException(String.format("Error: File has wrong format, provided file %s, the file should be in format ",
                    Files.probeContentType(path)));
        }

        if (Files.size(path) == 0) {
            throw new IOException(String.format("Error: Provided restore file is empty"));
        }

        try (Stream<String> resFileStream = Files.lines(path)) {
            resFileStream.forEach(line -> {
                dataStoreService.convertFromDataStoreFormat(line);
            });
        } catch (Exception ex) {
            throw new IOException(ex.getMessage());
        }
    }

    public void assertValidFileExtension(String filePath) throws IOException {
        val path = Paths.get(filePath);

        if (!Files.probeContentType(path).startsWith("text/")) {
            throw new IOException(String.format("Error: File has wrong format, provided file %s, the file should be in format ",
                    Files.probeContentType(path)));
        }
    }
}

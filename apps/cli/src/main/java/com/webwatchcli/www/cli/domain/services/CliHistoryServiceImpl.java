package com.webwatchcli.www.cli.domain.services;

import com.webwatchcli.www.cli.domain.exceptions.CliDataStoreReadException;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.hibernate.validator.constraints.URL;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.Set;

import static com.webwatchcli.www.utils.textmessages.UtilsValidationMessages.Url.NOT_VALID_URL_MSG;

@Log
@Service
@Validated
@RequiredArgsConstructor
public class CliHistoryServiceImpl implements CliHistoryService {

    private final CliDataStoreService dataStoreService;

    @Override
    public String history(@Valid Set<@URL(message = NOT_VALID_URL_MSG) String> urls) {
        try {
            return dataStoreService.readAll(urls);
        } catch (CliDataStoreReadException e) {
            return "Error: No datastore file exists.";
        }
    }
}

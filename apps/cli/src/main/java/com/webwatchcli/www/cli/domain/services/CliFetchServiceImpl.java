package com.webwatchcli.www.cli.domain.services;

import com.webwatchcli.www.cli.domain.models.CliWebWatchModel;
import com.webwatchcli.www.cli.domain.props.CliProperties;
import com.webwatchcli.www.cli.domain.utils.CliUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Log4j2
@Service
@RequiredArgsConstructor
public class CliFetchServiceImpl implements CliFetchService {

    private final WebClient webClient;
    private final CliProperties cliProperties;
    private final CliDataStoreService dataStoreService;

    @Override
    public String fetch(Set<String> urls, boolean print, int timout) {
        val watchModelSet = CliUtils.getUrlsToTest(cliProperties.urls(), new HashSet<>(urls))
                .stream()
                .parallel()
                .map(url -> webClient.get()
                        .uri(url)
                        .retrieve()
                        .toBodilessEntity()
                        .timeout(Duration.ofSeconds(timout), Mono.just(ResponseEntity.badRequest().build()))
                        .map(res -> new CliWebWatchModel(Instant.now().toEpochMilli(), url, res.getStatusCode().value()))
                        .block())
                .collect(Collectors.toSet());

        dataStoreService.persistAll(watchModelSet);

        if (print) {
            return CliUtils.printResult(watchModelSet);
        }

        return "Done!";
    }
}

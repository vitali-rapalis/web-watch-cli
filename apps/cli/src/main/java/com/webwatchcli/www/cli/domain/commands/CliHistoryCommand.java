package com.webwatchcli.www.cli.domain.commands;

import com.webwatchcli.www.cli.domain.services.CliHistoryService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.hibernate.validator.constraints.URL;
import org.springframework.shell.standard.ShellCommandGroup;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.util.Set;

import static com.webwatchcli.www.utils.textmessages.UtilsValidationMessages.Url.NOT_VALID_URL_MSG;


@Log4j2
@RequiredArgsConstructor
@ShellComponent("history_")
@ShellCommandGroup("Cli History Commands")
public class CliHistoryCommand {

    private final CliHistoryService historyService;

    @ShellMethod(key = "history_", value = "Outputs in a table like format all the data gathered in the data store")
    public String history(@ShellOption(value = {"--urls", "-u"},
            help = "URLs of configured website(s) or service(s) with comma separated, --urls https://google.com,https://amazon.com (optional argument)",
            defaultValue = ""
    ) Set<@URL(message = NOT_VALID_URL_MSG) String> urls) {
        return historyService.history(urls);
    }
}

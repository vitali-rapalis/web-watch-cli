package com.webwatchcli.www.cli.domain.commands;

import com.webwatchcli.www.cli.domain.services.CliConfigurationService;
import com.webwatchcli.www.utils.textmessages.UtilsValidationMessages;
import jakarta.validation.constraints.NotEmpty;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.URL;
import org.springframework.shell.standard.ShellCommandGroup;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.util.List;

import static com.webwatchcli.www.utils.textmessages.UtilsValidationMessages.Url.NOT_VALID_URL_MSG;

@RequiredArgsConstructor
@ShellComponent("config")
@ShellCommandGroup("Cli Configuration Commands")
public class CliConfigurationCommand {

    private final CliConfigurationService configurationService;

    @ShellMethod(key = "config add", value = "Add website/services urls")
    public String addUrl(@ShellOption(value = {"--urls", "-u"},
            help = "Add websites/services urls comma separated, --urls https://google.com,https://amazon.com"
    ) @NotEmpty List<@URL(message = NOT_VALID_URL_MSG) String> urls) {
        return configurationService.addUrls(urls);
    }

    @ShellMethod(key = "config remove", value = "Remove website/services urls")
    public String removeUrl(@ShellOption(value = {"--urls", "-u"},
            help = "Remove websites/services urls comma separated, --urls https://google.com,https://amazon.com"
    ) @NotEmpty List<@URL(message = NOT_VALID_URL_MSG) String> urls) {
        return configurationService.removeUrls(urls);
    }

    @ShellMethod(key = "config list", value = "List configuration")
    public String list() {
        return configurationService.list();
    }
}

package com.webwatchcli.www.cli.domain.commands;

import com.webwatchcli.www.cli.domain.services.CliFetchService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.hibernate.validator.constraints.URL;
import org.springframework.shell.standard.ShellCommandGroup;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;
import org.springframework.shell.standard.ShellOption;

import java.util.Set;

import static com.webwatchcli.www.utils.textmessages.UtilsValidationMessages.Url.NOT_VALID_URL_MSG;

@Log4j2
@RequiredArgsConstructor
@ShellComponent("fetch")
@ShellCommandGroup("Cli Fetch Commands")
public class CliFetchCommand {

    private final CliFetchService fetchService;

    @ShellMethod(key = "fetch", value = "Query all URLs of configured website(s) or service(s). " +
            "Output will be saved to datastore")
    public String fetch(@ShellOption(value = {"--urls", "-u"},
            help = "URLs of configured website(s) or service(s) with comma separated, --urls https://google.com,https://amazon.com (optional argument)",
            defaultValue = ""
    ) Set<@URL(message = NOT_VALID_URL_MSG) String> urls,
                        @ShellOption(value = {"--print", "-p"},
                                help = "Output the result of a website or service (optional argument)", defaultValue = "false") boolean print,
                        @ShellOption(value = {"--timout", "-t"},
                                help = "Timout in seconds (optional argument)", defaultValue = "3") int timout) {
        return fetchService.fetch(urls, print, timout);
    }
}

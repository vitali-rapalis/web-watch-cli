package com.webwatchcli.www.cli.domain.models;

import java.time.Instant;
import java.util.Objects;

public record CliWebWatchModel(long timestamp, String url, Integer status) implements Comparable<CliWebWatchModel> {

    public String printResult() {
        return String.format("%s \t| %s \t| %s", url, status, Instant.ofEpochMilli(timestamp));
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CliWebWatchModel that = (CliWebWatchModel) o;
        return timestamp == that.timestamp && Objects.equals(url, that.url) && Objects.equals(status, that.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(timestamp, url, status);
    }

    @Override
    public int compareTo(CliWebWatchModel other) {
        return Long.compare(this.timestamp, other.timestamp);
    }
}

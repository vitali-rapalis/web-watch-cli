package com.webwatchcli.www.cli.domain.services;

import com.webwatchcli.www.cli.domain.models.CliWebWatchModel;
import com.webwatchcli.www.cli.domain.props.CliProperties;
import com.webwatchcli.www.cli.domain.utils.CliUtils;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.Duration;
import java.time.Instant;
import java.util.HashSet;
import java.util.List;

@Log4j2
@Service
@RequiredArgsConstructor
public class CliLiveServiceImpl implements CliLiveService {

    private final WebClient webClient;
    private final CliDataStoreService dataStoreService;
    private final CliProperties cliProperties;

    @Override
    public void live(List<String> urls, int interval, boolean print, int timeout) {
        val urlsToTestSet = CliUtils.getUrlsToTest(cliProperties.urls(), new HashSet<>(urls));

        Flux.interval(Duration.ofSeconds(interval))
                .flatMap(aLong -> Flux.fromIterable(urlsToTestSet))
                .flatMap(url -> webClient.get()
                        .uri(url)
                        .retrieve()
                        .toBodilessEntity()
                        .timeout(Duration.ofSeconds(timeout), Mono.just(ResponseEntity.badRequest().build()))
                        .map(res -> new CliWebWatchModel(Instant.now().toEpochMilli(), url, res.getStatusCode().value())))
                .doFirst(() -> CliUtils.printResult())
                .doOnNext(modelToPersist -> dataStoreService.persist(modelToPersist))
                .filter(__ -> print == true)
                .doOnNext(CliUtils::printResult)
                .take(Duration.ofMinutes(10))
                .subscribe();
    }
}

package com.webwatchcli.www.cli.domain.services;

import java.util.Set;

public interface CliFetchService {

    String fetch(Set<String> urls, boolean print, int timout);
}

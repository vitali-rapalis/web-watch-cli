package com.webwatchcli.www.cli.domain.services;

import java.util.List;

public interface CliLiveService {

    void live(List<String> urls, int interval, boolean print, int timeout);
}

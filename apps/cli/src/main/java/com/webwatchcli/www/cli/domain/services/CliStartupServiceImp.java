package com.webwatchcli.www.cli.domain.services;

import jakarta.annotation.PostConstruct;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;


@Log4j2
@Service
@RequiredArgsConstructor
public class CliStartupServiceImp implements CliStartupService {

    private final CliDataStoreService cliDataStoreService;

    @Override
    @PostConstruct
    public void startUp() {

        cliDataStoreService.createDataStoreFile();
    }
}

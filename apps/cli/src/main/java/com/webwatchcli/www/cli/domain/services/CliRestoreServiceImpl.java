package com.webwatchcli.www.cli.domain.services;

import com.webwatchcli.www.cli.domain.props.CliProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Log4j2
@Service
@RequiredArgsConstructor
public class CliRestoreServiceImpl implements CliRestoreService {

    private final CliProperties cliProperties;
    private final CliDataStoreService dataStoreService;
    private final CliAssertionService cliAssertionService;

    @Override
    public String restore(String filePath) {
        try {
            cliAssertionService.assertFileIsValid(filePath);
        } catch (IOException e) {
            return e.getMessage();
        }

        try (Stream<String> dataStoreFileStream = Files.lines(cliProperties.getDataStoreFilePath());
             Stream<String> restoreFileStream = Files.lines(Paths.get(filePath))) {

            val dataStoreSet = dataStoreFileStream
                    .parallel()
                    .map(dataStoreService::convertFromDataStoreFormat)
                    .collect(Collectors.toSet());

            val restoreFileSet = restoreFileStream
                    .parallel()
                    .map(dataStoreService::convertFromDataStoreFormat)
                    .collect(Collectors.toSet());

            if (Files.notExists(cliProperties.getTmpBackupFilePath())) {
                Files.createFile(cliProperties.getTmpBackupFilePath());
            }

            Files.copy(cliProperties.getDataStoreFilePath(), cliProperties.getTmpBackupFilePath(), StandardCopyOption.REPLACE_EXISTING);

            if (Files.exists(cliProperties.getDataStoreFilePath())) {
                Files.delete(cliProperties.getDataStoreFilePath());
            }

            Files.createFile(cliProperties.getDataStoreFilePath());

            val sortedNewSetSet = new TreeSet<>(restoreFileSet);
            sortedNewSetSet.addAll(dataStoreSet);

            sortedNewSetSet.stream().forEach(dataStoreService::persist);

            return "Done!";
        } catch (Exception ex) {
            try {
                Files.copy(cliProperties.getTmpBackupFilePath(), cliProperties.getDataStoreFilePath(), StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                log.error("Error: Restore datastore file failed, reason: {}", e.getMessage());
            }
            return String.format("Error: Restore file failed, reason: %s", ex.getMessage());
        }
    }
}

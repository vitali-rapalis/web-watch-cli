package com.webwatchcli.www.cli.domain.props;

import jakarta.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.URL;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

import java.nio.file.Path;
import java.util.Set;

@Validated
@ConfigurationProperties("web-watch-cli")
public record CliProperties(@NotEmpty Set<@URL(message = "Invalid URL") String> urls,
                            @Length(min = 3, max = 15) String promptName,
                            @NotEmpty String cliHomeDirPath,
                            @NotEmpty String dataStoreFilePath,
                            @Length(min = 3, max = 80) String tmpBackupFilePath) {

    public String printUrls() {
        return """
                Configured urls:
                ------------
                %s                
                """.formatted(urls.toString());
    }

    public Path getCliHomeDirPath() {
        return Path.of(cliHomeDirPath);
    }

    public Path getDataStoreFilePath() {
        return Path.of(dataStoreFilePath);
    }

    public Path getTmpBackupFilePath() {
        return Path.of(tmpBackupFilePath);
    }
}

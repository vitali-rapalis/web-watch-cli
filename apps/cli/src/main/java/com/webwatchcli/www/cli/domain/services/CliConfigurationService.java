package com.webwatchcli.www.cli.domain.services;

import com.webwatchcli.www.cli.domain.props.CliProperties;
import com.webwatchcli.www.cli.domain.utils.CliUtils;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import org.hibernate.validator.constraints.URL;
import org.springframework.stereotype.Service;
import org.springframework.validation.annotation.Validated;

import java.util.List;

import static com.webwatchcli.www.utils.textmessages.UtilsValidationMessages.Url.NOT_VALID_URL_MSG;

@Log
@Service
@Validated
@RequiredArgsConstructor
public class CliConfigurationService {

    private final CliProperties cliProperties;

    public String addUrls(@Valid @NotEmpty List<@URL(message = NOT_VALID_URL_MSG) String> urls) {
        cliProperties.urls().addAll(urls);
        return cliProperties.printUrls();
    }

    public String removeUrls(@Valid @NotEmpty List<@URL(message = NOT_VALID_URL_MSG) String> urls) {
        var isRemoved = cliProperties.urls().removeAll(urls);
        if (isRemoved) {
            return cliProperties.printUrls();
        } else {
            return CliUtils.PROVIDED_URLS_NOT_FOUND_MSG;
        }
    }

    public String list() {
        return String.format("""
                CONFIGURATION:
                -------------
                PRECONFIGURED URLS:
                %s \n
                DATASTORE FILE PATH:
                %s
                """, cliProperties.urls(),cliProperties.dataStoreFilePath());
    }
}

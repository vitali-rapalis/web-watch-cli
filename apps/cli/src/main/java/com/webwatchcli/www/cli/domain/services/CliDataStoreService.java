package com.webwatchcli.www.cli.domain.services;

import com.webwatchcli.www.cli.domain.exceptions.CliDataStorePersistenceException;
import com.webwatchcli.www.cli.domain.exceptions.CliDataStoreReadException;
import com.webwatchcli.www.cli.domain.models.CliWebWatchModel;

import java.util.IllegalFormatException;
import java.util.Set;

public interface CliDataStoreService {

    void createDataStoreFile();

    public String convertToDataStoreFormat(CliWebWatchModel model);

    public CliWebWatchModel convertFromDataStoreFormat(String line) throws IllegalArgumentException;

    void persistAll(Set<CliWebWatchModel> watchModelSet) throws CliDataStorePersistenceException;

    void persist(CliWebWatchModel watchModel) throws CliDataStorePersistenceException;

    String readAll(Set<String> urls) throws CliDataStoreReadException;
}

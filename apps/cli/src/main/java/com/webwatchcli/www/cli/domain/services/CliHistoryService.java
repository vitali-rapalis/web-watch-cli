package com.webwatchcli.www.cli.domain.services;


import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import org.hibernate.validator.constraints.URL;

import java.util.Set;

import static com.webwatchcli.www.utils.textmessages.UtilsValidationMessages.Url.NOT_VALID_URL_MSG;

public interface CliHistoryService {

    String history(@Valid Set<@URL(message = NOT_VALID_URL_MSG) String> urls);
}

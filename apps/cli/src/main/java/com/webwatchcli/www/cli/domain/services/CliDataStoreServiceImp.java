package com.webwatchcli.www.cli.domain.services;

import com.webwatchcli.www.cli.domain.exceptions.CliDataStorePersistenceException;
import com.webwatchcli.www.cli.domain.exceptions.CliDataStoreReadException;
import com.webwatchcli.www.cli.domain.models.CliWebWatchModel;
import com.webwatchcli.www.cli.domain.props.CliProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import lombok.val;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.StandardOpenOption;
import java.util.Set;
import java.util.stream.Stream;

@Log4j2
@Service
@RequiredArgsConstructor
public class CliDataStoreServiceImp implements CliDataStoreService {

    private final CliProperties cliProperties;

    @Override
    public void createDataStoreFile() {
        log.info("Web watch cli home dir path is: {}", cliProperties.cliHomeDirPath());

        val cliHomeDir = new File(cliProperties.cliHomeDirPath());
        if (!cliHomeDir.exists()) {
            cliHomeDir.mkdir();
            log.info("Web cli home dir created at path: {}", cliHomeDir.getAbsolutePath());
        } else {
            log.info("Web cli home dir exists at path: {}", cliHomeDir.getAbsolutePath());
        }

        val file = new File(cliProperties.dataStoreFilePath());
        if (!file.exists()) {
            try {
                file.createNewFile();
                log.info("File created on path: " + file.getAbsolutePath());
            } catch (IOException e) {
                log.error("Web cli datastore file could not be created, reason: {}", e.getMessage());
                System.exit(0);
            }
        }
    }

    @Override
    public void persistAll(Set<CliWebWatchModel> watchModelSet) throws CliDataStorePersistenceException {
        val models = watchModelSet.stream()
                .map(this::convertToDataStoreFormat)
                .reduce("", (result, next) -> result + next + "\n");
        writeToFile(models);
    }

    @Override
    public void persist(CliWebWatchModel watchModel) throws CliDataStorePersistenceException {
        var convertedModel = convertToDataStoreFormat(watchModel);
        writeToFile(convertedModel + "\n");
    }

    @Override
    public String readAll(Set<String> urls) throws CliDataStoreReadException {
        try (Stream<String> stream = Files.lines(cliProperties.getDataStoreFilePath())) {
            return stream
                    .filter(line -> {
                        if (urls.isEmpty()) {
                            return true;
                        } else {
                            return urls.stream()
                                    .filter(givenWebsite -> givenWebsite.equals(line.split(";")[0]))
                                    .findFirst()
                                    .isPresent();
                        }
                    })
                    .reduce("RESULT: \n-------\n",
                            (result, nextLine) -> result + nextLine + "\n").toString();
        } catch (Exception ex) {
            log.error("Error: Datastore read operation failed, reason {}", ex.getMessage());
            throw new CliDataStoreReadException();
        }
    }

    private void writeToFile(String models) {
        try {
            Files.write(cliProperties.getDataStoreFilePath(), models.getBytes(StandardCharsets.UTF_8), StandardOpenOption.APPEND);
        } catch (Exception ex) {
            log.error("Persists data to store failed, reason: {}", ex.getMessage());
            throw new CliDataStorePersistenceException();
        }
    }

    public String convertToDataStoreFormat(CliWebWatchModel model) {
        return model.url() + ";" + model.status() + ";" + model.timestamp();
    }

    @Override
    public CliWebWatchModel convertFromDataStoreFormat(String line) throws IllegalArgumentException {
        val split = line.split(";");
        val urlString = split[0];
        val statusString = split[1];
        val timestampString = split[2];

        int status;
        long timestamp;

        if (split.length > 3) {
            throw new IllegalArgumentException("Error: Each line should contain only 3 columns ex. column1;column2;column3");
        }

        try {
            URL url = new URL(urlString);
            url.toURI();
        } catch (Exception ex) {
            throw new IllegalArgumentException(String.format("Error: illegal argument in line %s, reason: %s" +
                            "\ncolumn URL = %s is not convertable to URL ",
                    line, ex.getMessage(), timestampString));
        }

        try {
            timestamp = Long.parseLong(timestampString);
        } catch (Exception ex) {
            throw new IllegalArgumentException(String.format("Error: illegal argument in line %s, reason: %s" +
                            "\ncolumn TIMESTAMP = %s is not convertable to long ",
                    line, ex.getMessage(), timestampString));
        }

        try {
            status = Integer.parseInt(statusString);
        } catch (Exception ex) {
            throw new IllegalArgumentException(String.format("Error: illegal argument in line %s, reason: %s" +
                            "\ncolumn STATUS = %s is not convertable to int",
                    line, ex.getMessage(), statusString));
        }

        return new CliWebWatchModel(timestamp, urlString, status);
    }
}

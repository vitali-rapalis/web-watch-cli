package com.webwatchcli.www.cli.domain.commands;

import com.webwatchcli.www.cli.domain.services.CliBackupService;
import lombok.val;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.beans.factory.annotation.Autowired;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

@DisplayName("Cli restore shell command test")
public class CliRestoreCommandTest extends CliAbstractCommandTest {

    @Autowired
    private CliBackupService cliBackupService;

    @Test
    void restoreShouldMergeToDatastoreBySkipExistingRecordsTest(@TempDir Path restoreDirPath) throws IOException {
        assertDatastoreFileIsEmpty();
        shellScreenPrintedTextAbstractTest("fetch -p", "200", true);
        assertDatastoreIsNotEmpty();

        val restoreFilePath = Path.of(restoreDirPath + File.separator + "restore.txt");
        cliBackupService.backup(restoreFilePath.toString());
        val restoreUniqueRecord = "https://facebook.com;200;1082074898898";
        Files.write(restoreFilePath, restoreUniqueRecord.getBytes(), StandardOpenOption.APPEND);
        Assertions.assertThat(Files.size(restoreFilePath)).isGreaterThan(0);

        shellScreenPrintedTextAbstractTest("restore -f " + restoreFilePath.toString(), "Done!", true);
        val recordsFromDataStore = getRecordsFromDatastore();
        val uniqueRecordFromRestoreFile = dataStoreService.convertFromDataStoreFormat(restoreUniqueRecord);
        Assertions.assertThat(recordsFromDataStore.get(0).timestamp()).isEqualTo(uniqueRecordFromRestoreFile.timestamp());
    }

    @Test
    void restoreShouldFailedForProvidingNotValidRestoreFile(@TempDir Path restoreDirPath) throws IOException {
        val restoreFilePath = Path.of(restoreDirPath + File.separator + "restore.txt");
        val notValidStatusFormat = "bum";
        val restoreUniqueRecord = "https://facebook.com;" + notValidStatusFormat + ";1082074898898";
        Files.write(restoreFilePath, restoreUniqueRecord.getBytes(), StandardOpenOption.CREATE_NEW);

        shellScreenPrintedTextAbstractTest("restore -f " + restoreFilePath.toString(), "Error: illegal argument", true);
        shellScreenPrintedTextAbstractTest("restore -f " + restoreFilePath.toString(), "column STATUS = bum is not convertable to int", true);
    }
}

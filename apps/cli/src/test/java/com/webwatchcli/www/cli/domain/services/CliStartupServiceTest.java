package com.webwatchcli.www.cli.domain.services;


import com.webwatchcli.www.cli.config.CliComponentTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;

@CliComponentTest
@Import(CliStartupServiceImp.class)
@DisplayName("Cli startup service test")
class CliStartupServiceTest implements CliStartupService {

    @Autowired
    private CliStartupService startupService;

    @MockBean
    private CliDataStoreService dataStoreServiceMock;


    @Override
    @Test
    @DisplayName("Verify that dataStoreService call create data store file after post construct test")
    public void startUp() {
        Mockito.doNothing().when(dataStoreServiceMock).createDataStoreFile();

        Mockito.verify(dataStoreServiceMock, Mockito.times(1)).createDataStoreFile();
    }
}
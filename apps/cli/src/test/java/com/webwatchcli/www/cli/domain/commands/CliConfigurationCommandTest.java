package com.webwatchcli.www.cli.domain.commands;

import lombok.val;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static com.webwatchcli.www.utils.textmessages.UtilsValidationMessages.Url.NOT_VALID_URL_MSG;

@DisplayName("Cli configuration shell command test")
public class CliConfigurationCommandTest extends CliAbstractCommandTest {

    @Test
    void addNotValidUrlsShouldPrintExceptionMsgOnScreenTest() {
        shellScreenPrintedTextAbstractTest(String.format("config add --urls %s", "not_valid.url.format"), NOT_VALID_URL_MSG, true);
    }

    @Test
    void addShouldPrintUrlsOnScreenTest() {
        // Given
        val validUrl = "https://microsoft.com";

        // Then
        shellScreenPrintedTextAbstractTest(String.format("config add --urls %s", validUrl), validUrl, true);
    }

    @Test
    void removeNotValidUrlsShouldPrintExceptionMsgOnScreenTest() {
        shellScreenPrintedTextAbstractTest(String.format("config remove --urls %s", "not_valid.url.format"), NOT_VALID_URL_MSG, true);
    }

    @Test
    void removeShouldPrintConfiguredUrlsOnScreenTest() {
        // Given
        var configuredUrl = "https://aol.com";
        shellScreenPrintedTextAbstractTest(String.format("config add --urls %s", configuredUrl), configuredUrl, true);

        // Then
        shellScreenPrintedTextAbstractTest(String.format("config remove --urls %s", configuredUrl), configuredUrl, false);
    }

    @Test
    void listConfigurationCommandTest() {
        shellScreenPrintedTextAbstractTest("config list", "CONFIGURATION", true);
    }
}

package com.webwatchcli.www.cli.domain.commands;

import com.webwatchcli.www.cli.config.CliShellCommandTest;
import com.webwatchcli.www.cli.domain.models.CliWebWatchModel;
import com.webwatchcli.www.cli.domain.props.CliProperties;
import com.webwatchcli.www.cli.domain.services.CliDataStoreService;
import com.webwatchcli.www.cli.domain.services.CliStartupService;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.shell.test.ShellAssertions;
import org.springframework.shell.test.ShellTestClient;

import java.io.IOException;
import java.nio.file.Files;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static org.awaitility.Awaitility.await;

@CliShellCommandTest
abstract class CliAbstractCommandTest {

    private final static long DEFAULT_TIMEOUT_SECONDS = 5;

    @Autowired
    protected ShellTestClient client;

    @Autowired
    protected CliStartupService startupService;

    @Autowired
    protected CliProperties cliProperties;

    @Autowired
    protected CliDataStoreService dataStoreService;

    @BeforeEach
    void setUp() {
        startupService.startUp();
    }

    @AfterEach
    void tearDown() {
        try {
            Files.deleteIfExists(cliProperties.getDataStoreFilePath());
        } catch (IOException e) {

        }
    }

    protected void shellScreenPrintedTextAbstractTest(String shellCommand, String printedMsg, boolean shouldTextBeContained) {
        shellScreenPrintedTextAbstractTest(shellCommand, printedMsg, shouldTextBeContained, DEFAULT_TIMEOUT_SECONDS);
    }

    protected void shellScreenPrintedTextAbstractTest(String shellCommand, String printedMsg, boolean shouldTextBeContained, long timeoutSeconds) {
        // When
        ShellTestClient.NonInteractiveShellSession session = client
                .nonInterative(shellCommand.split(" "))
                .run();

        // Then
        await().atMost(timeoutSeconds, TimeUnit.SECONDS).untilAsserted(() -> {
            if (shouldTextBeContained) {
                ShellAssertions.assertThat(session.screen())
                        .containsText(printedMsg);
            } else {
                ShellAssertions.assertThat(session.screen())
                        .doesNotHaveToString(printedMsg);
            }
        });
    }

    protected void assertDatastoreFileIsEmpty() throws IOException {
        Assertions.assertThat(Files.size(cliProperties.getDataStoreFilePath())).isEqualTo(0);
    }

    protected void assertDatastoreIsNotEmpty() throws IOException {
        Assertions.assertThat(Files.size(cliProperties.getDataStoreFilePath())).isGreaterThan(0);
    }

    protected List<CliWebWatchModel> getRecordsFromDatastore() throws IOException {
        return Files.lines(cliProperties.getDataStoreFilePath())
                .map(dataStoreService::convertFromDataStoreFormat)
                .toList();
    }
}

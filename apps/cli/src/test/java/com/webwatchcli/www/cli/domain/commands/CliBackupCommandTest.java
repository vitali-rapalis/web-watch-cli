package com.webwatchcli.www.cli.domain.commands;

import lombok.val;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

@DisplayName("Cli backup shell command test")
public class CliBackupCommandTest extends CliAbstractCommandTest {

    @Test
    void backupTest(@TempDir Path backupFileDirPath) throws IOException {
        assertDatastoreFileIsEmpty();
        shellScreenPrintedTextAbstractTest("fetch -p", "RESULT", true);
        assertDatastoreIsNotEmpty();

        val backupFilePath = Path.of(backupFileDirPath + File.separator + "backup.txt");
        shellScreenPrintedTextAbstractTest("backup -f " + backupFilePath, "Backup created", true);
        Assertions.assertThat(Files.size(backupFilePath)).isGreaterThan(0);
        Files.lines(backupFilePath)
                .map(dataStoreService::convertFromDataStoreFormat)
                .forEach(record -> Assertions.assertThat(record.status()).isEqualTo(200));
    }

    @Test
    void backupFailedByProvidingNotTxtFileTest(@TempDir Path backupFileDirPath) throws IOException {
        assertDatastoreFileIsEmpty();
        shellScreenPrintedTextAbstractTest("fetch -p", "RESULT", true);
        assertDatastoreIsNotEmpty();

        val backupFilePath = Path.of(backupFileDirPath + File.separator + "backup.bin");
        shellScreenPrintedTextAbstractTest("backup -f " + backupFilePath, "Backup failed", true);
    }
}

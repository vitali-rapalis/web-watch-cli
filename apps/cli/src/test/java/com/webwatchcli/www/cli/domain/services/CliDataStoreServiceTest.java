package com.webwatchcli.www.cli.domain.services;

import com.webwatchcli.www.cli.config.CliComponentTest;
import com.webwatchcli.www.cli.domain.props.CliProperties;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

@CliComponentTest
@Import({CliDataStoreServiceImp.class})
@DisplayName("Cli startup service test")
public class CliDataStoreServiceTest {

    @Autowired
    private CliDataStoreService dataStoreService;

    @Autowired
    private CliProperties cliProperties;

    @BeforeEach
    void setUp() throws IOException {
        deleteCreatedFiles();
    }

    @Test
    void shouldCreateDataStoreFileOnStartUpTest() throws IOException {
            dataStoreService.createDataStoreFile();
            assertDataStoreFileIsCreated();
    }

    @Test
    void shouldNotRecreateDatastoreIfDataStoreExistsTest() throws IOException {
            dataStoreService.createDataStoreFile();
            assertDataStoreFileIsCreated();
            var storeData = "store data";
            Files.write(cliProperties.getDataStoreFilePath(), storeData.getBytes(StandardCharsets.UTF_8));

            dataStoreService.createDataStoreFile();
            var readDataFromStore = Files.readString(cliProperties.getDataStoreFilePath());
            Assertions.assertThat(storeData).isEqualTo(readDataFromStore);
    }

    @AfterEach
    void tearDown() throws IOException {
        deleteCreatedFiles();
    }

    private void deleteCreatedFiles() throws IOException {
        Files.deleteIfExists(cliProperties.getDataStoreFilePath());
        Files.deleteIfExists(cliProperties.getCliHomeDirPath());
    }

    private void assertDataStoreFileIsCreated() {
        Assertions.assertThat(Files.exists(cliProperties.getDataStoreFilePath())).isTrue();
    }
}

package com.webwatchcli.www.cli.domain.services;

import com.webwatchcli.www.cli.config.CliComponentTest;
import com.webwatchcli.www.cli.domain.utils.CliUtils;
import jakarta.validation.ConstraintViolationException;
import lombok.val;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Import;

import java.util.List;

import static com.webwatchcli.www.utils.textmessages.UtilsValidationMessages.Url.NOT_VALID_URL_MSG;

@CliComponentTest
@Import(CliConfigurationService.class)
@DisplayName("Cli service configuration test")
public class CliConfigurationServiceTest {

    @Autowired
    private CliConfigurationService configurationService;

    @Test
    void notValidParameterShouldFailTest() {
        Assertions.assertThatThrownBy(() -> configurationService.addUrls(List.of("bum")))
                .hasMessageContaining(NOT_VALID_URL_MSG)
                .isExactlyInstanceOf(ConstraintViolationException.class);
    }

    @Test
    void shouldPrintUrlsTest() {
        // Given
        val url = "https://aol.de";

        // When
        val printedUrl = configurationService.addUrls(List.of(url));

        // Then
        Assertions.assertThat(printedUrl).contains(url);
    }

    @Test
    void shouldRemovePreconfiguredUrlsTest() {
        // Given
        val givenUrl = "https://aol.com";
        val givenUrls = List.of(givenUrl);

        // When
        val addedUrls = configurationService.addUrls(givenUrls);
        Assertions.assertThat(addedUrls).contains(givenUrl);
        val configuredUrls = configurationService.removeUrls(givenUrls);

        // Then
        Assertions.assertThat(configuredUrls).doesNotContain(givenUrls);
    }

    @Test
    void shouldThrowConstrainsViolationForNotValidUrlTest() {
        Assertions.assertThatThrownBy(() -> configurationService.removeUrls(List.of("bum")))
                .hasMessageContaining(NOT_VALID_URL_MSG)
                .isExactlyInstanceOf(ConstraintViolationException.class);
    }

    @Test
    void removeNotConfiguredUrlsShouldPrintMsgTest() {
        // Given
        val notExistingUrl = "https://aol.com";

        // When
        val printMsg = configurationService.removeUrls(List.of(notExistingUrl));

        // Then
        Assertions.assertThat(printMsg).contains(CliUtils.PROVIDED_URLS_NOT_FOUND_MSG);
    }
}

package com.webwatchcli.www.cli.config;

import com.webwatchcli.www.cli.domain.services.*;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import org.springframework.shell.test.autoconfigure.ShellTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.TestPropertySource;

import java.lang.annotation.*;


@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@EnableAutoConfiguration
@Import({CliConfiguration.class, CliBackupServiceImpl.class, CliFetchServiceImpl.class, CliDataStoreServiceImp.class,
        CliHistoryServiceImpl.class, CliLiveServiceImpl.class, CliRestoreServiceImpl.class, CliAssertionService.class,
        CliConfigurationService.class, CliStartupServiceImp.class})
@TestPropertySource(properties = {
        "web-watch-cli.urls[0]=https://google.com",
        "web-watch-cli.urls[1]=https://google.com",
        "web-watch-cli.urls[2]=https://amazon.com",
        "web-watch-cli.prompt-name=web-watch-cli>"
})
@ShellTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public @interface CliShellCommandTest {

}

package com.webwatchcli.www.cli.domain.commands;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.IOException;

@DisplayName("Cli history shell command test")
public class CliHistoryCommandTest extends CliAbstractCommandTest {

    @Test
    void historyCommandShouldPrintResultFromDatastoreTest() throws IOException {
        assertDatastoreFileIsEmpty();
        shellScreenPrintedTextAbstractTest("fetch -p", "RESULT", true);
        assertDatastoreIsNotEmpty();

        shellScreenPrintedTextAbstractTest("history", "https://google.com", true);
    }
}

package com.webwatchcli.www.cli.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.test.context.SpringBootTest;

import java.lang.annotation.*;

@Documented
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@EnableAutoConfiguration
@SpringBootTest(properties = {
        "web-watch-cli.urls[0]=https://google.com",
        "web-watch-cli.urls[1]=https://google.com",
        "web-watch-cli.urls[1]=https://amazon.com"
}, classes = {CliConfiguration.class})
public @interface CliComponentTest {
}

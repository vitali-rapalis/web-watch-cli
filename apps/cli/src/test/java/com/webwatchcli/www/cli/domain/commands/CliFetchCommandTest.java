package com.webwatchcli.www.cli.domain.commands;

import lombok.val;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;
import java.nio.file.Files;

import static com.webwatchcli.www.utils.textmessages.UtilsValidationMessages.Url.NOT_VALID_URL_MSG;

@TestPropertySource(properties = {
        "web-watch-cli.urls[0]=https://google.com",
        "web-watch-cli.urls[1]=https://amazon.com",
        "web-watch-cli.urls[2]=https://hub.dummyapis.com/delay?seconds=4",
})
@DisplayName("Cli fetch shell command test")
public class CliFetchCommandTest extends CliAbstractCommandTest {

    @Test
    void shouldPrintResultOnShellTest() {
        shellScreenPrintedTextAbstractTest("fetch -p", "RESULT", true);
    }

    @Test
    void shouldQuerySubsetOfUrlsTest() {
        val queriedUrl = "https://google.com";
        val notQueriedUrl = "https://amazon.com";

        shellScreenPrintedTextAbstractTest(String.format("fetch -p -u %s", queriedUrl), queriedUrl, true);
        shellScreenPrintedTextAbstractTest(String.format("fetch -p -u %s", queriedUrl), notQueriedUrl, false);
    }

    @Test
    void shouldQueryWithDefaultTimeoutParameter5SecHttpStatusShouldBe400Test() {
        val slowApiUrl = "https://hub.dummyapis.com/delay?seconds=4";
        shellScreenPrintedTextAbstractTest(String.format("fetch -p -u %s", slowApiUrl), "400", true);
    }

    @Test
    void shouldQueryWithDefaultTimeoutParameter10SecHttpStatusShouldBe200Test() {
        val timout = 5;
        val slowApiUrl = "https://hub.dummyapis.com/delay?seconds=4";
        shellScreenPrintedTextAbstractTest(String.format("fetch -p -u %s -t %s", slowApiUrl, timout), "200", true);
    }

    @Test
    void shouldStoreResultToDatastoreFileTest() throws IOException {
        assertDatastoreFileIsEmpty();

        val timout = 5;
        val slowApiUrl = "https://hub.dummyapis.com/delay?seconds=4";
        shellScreenPrintedTextAbstractTest(String.format("fetch -p -u %s -t %s", slowApiUrl, timout), "200", true);

        assertDatastoreIsNotEmpty();

        Files.lines(cliProperties.getDataStoreFilePath())
                .forEach(line -> Assertions.assertThat(line).contains("200"));
    }

    @Test
    void shouldForNotValidUrlsProvidedPrintErrorMsgTest() {
        shellScreenPrintedTextAbstractTest(String.format("config add --urls %s", "not_valid.url.format"), NOT_VALID_URL_MSG, true);
    }
}

package com.webwatchcli.www.cli.domain.commands;

import lombok.val;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.TestPropertySource;

import java.io.IOException;

@TestPropertySource(properties = {
        "web-watch-cli.urls[0]=https://google.com",
        "web-watch-cli.urls[1]=https://hub.dummyapis.com/delay?seconds=4"
})
@DisplayName("Cli live shell command test")
public class CliLiveCommandTest extends CliAbstractCommandTest {

    @Test
    void shouldCreateRecordsInDatastoreAccordingIntervalTest() throws IOException, InterruptedException {
        assertDatastoreFileIsEmpty();
        val queriedUrl = "https://google.com";
        shellScreenPrintedTextAbstractTest("live -i 1 -p -u " + queriedUrl, "", false);

        Thread.sleep(5000);

        assertDatastoreIsNotEmpty();
        val recordsFromDatastore = getRecordsFromDatastore();
        Assertions.assertThat(recordsFromDatastore.size()).isBetween(4, 5);
    }

    @Test
    void shouldGetTimeoutForLongRunningResponsesTest() throws Exception {
        assertDatastoreFileIsEmpty();

        val queriedUrl = "https://hub.dummyapis.com/delay?seconds=3";
        shellScreenPrintedTextAbstractTest("live -i 5 -p -u " + queriedUrl, "", false);

        Thread.sleep(5000);
        val recordsFromDatastore = getRecordsFromDatastore();
        recordsFromDatastore.forEach(record -> Assertions.assertThat(record.status()).isEqualTo(400));
    }

    @Test
    void shouldGetNoTimeoutForLongRunningResponsesBySpecifyingTimoutDurationTest() throws Exception {
        assertDatastoreFileIsEmpty();

        val queriedUrl = "https://hub.dummyapis.com/delay?seconds=3";
        shellScreenPrintedTextAbstractTest("live -i 5 -p -u " + queriedUrl + "-t " + 4, "", false);

        Thread.sleep(5000);
        val recordsFromDatastore = getRecordsFromDatastore();
        recordsFromDatastore.forEach(record -> Assertions.assertThat(record.status()).isEqualTo(200));
    }
}

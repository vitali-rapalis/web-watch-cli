package com.webwatchcli.www.cli.domain.props;

import com.webwatchcli.www.cli.config.CliComponentTest;
import lombok.val;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

@CliComponentTest
@DisplayName("Cli props test")
public final class CliPropertiesTest {

    @Autowired
    private CliProperties cliProperties;

    @Test
    @DisplayName("Urls should not be repeated")
    void urlsShouldNotBeRepeated() {
        // Given
        val urls = cliProperties.urls();

        // Then
        Assertions.assertThat(urls).doesNotHaveDuplicates();
    }

    @Test
    void promptNameShouldBeNotNullTest() {
        // Given
        val promptName = cliProperties.promptName();

        // Then
        Assertions.assertThat(promptName).isNotNull();
    }
}

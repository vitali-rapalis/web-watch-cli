package com.webwatchcli.www.utils.textmessages;

import lombok.experimental.UtilityClass;

@UtilityClass
public class UtilsValidationMessages {

    public static class Url {
        public static final String NOT_VALID_URL_MSG = "Not valid url format";
    }
}

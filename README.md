[![Java version](https://img.shields.io/badge/Java-17-blue)](https://img.shields.io/badge/Java-17-blue)
[![Gradle version](https://img.shields.io/badge/Gradle-8-green)](https://img.shields.io/badge/Gradle-8-green)
[![Spring version](https://img.shields.io/badge/Spring%20Shell-3.0.2-brightgreen)](https://img.shields.io/badge/Spring%20Shell-3.0.2-brightgreen)
[![CI/CD status](https://gitlab.com/vitali-rapalis/web-watch-cli/badges/main/pipeline.svg)](https://gitlab.com/vitali-rapalis/web-watch-cli/commits/main)

# Web Watch CLI Tool

---

> **Web Watch CLI:** The Ultimate CLI Tool for Testing Website/Service Availability :) <br/>
> [Project can be found at **GitLab** repository](https://gitlab.com/vitali-rapalis/web-watch-cli)
## Info

The CLI tool for website and service availability testing. The tool is written in **Java 17**, using
the **Gradle 8** build system and **Spring Shell 3.0.2** framework.

### How to Run

- Easy way by running **docker** <br/>

Run docker container:

```
docker run -it --rm vrapalis/web-watch-cli sh
```

Type in container bind terminal:

```
web-watch-cli
```

Example output:
![Docker](docs/screenshots/docker-container-run.png)

- Another way by running **gradle** <br/>

Gradle build task on *linux* jar file **cli-0.0.1.jar** will be generated in build/libs directory

```
./gradlew :apps:cli:build
```

Gradle build task on *windows* jar file **cli-0.0.1.jar** will be generated in build/libs directory

```
gradlew.bat :apps:cli:build
```

Execute generated jar file

```
java -jar project-path/build/libs/cli-0.0.1.jar
```

To enable log

```
java -jar -Dlogging.level.root=info project-path/build/libs/cli-0.0.1.jar
```

## CLI Commands

- **help**, list all available commands
  ![Help](docs/screenshots/help-command.png)

- **config**, configure and print configuration command <br/>
**list** will print cli configuration
```
config list 
```

Example output
![Config List](docs/screenshots/config-list.png)

**config list --help** --help can be used to get detail information about command
```
config list --help
```
Example output
![Config List Help](docs/screenshots/confg-list-help.png)

**add -u website_url,website_url** command will add website to preconfigured websites pool
```
config add -u https://yahoo.com
```
Example output
![Config Add](docs/screenshots/config-add.png)

**remove -u website_url,website_url** will remove website/s from preconfigured pool
```
config remove -u https://facebook.com
```
Example output
![Config remove](docs/screenshots/config-remove.png)

- **fetch** command will test websites/services
Possible parameters -u (Provide pool of tested urls) -p (Print the result) -t (Specified timeout) -h (For help)
```
fetch -p -t 4
```
Example output
![Fetch](docs/screenshots/fetch.png)

- **live** will test the website on interval
```
live -p -i 5 -t 6
```
Example output
![Live](docs/screenshots/live.png)

- **history_** will print data from datastore
```
history_ -u https://google.com
```
Example output
![History](docs/screenshots/history.png)

- **backup** command, takes a file path argument and creates a backup of data in current data store
```
backup -f /home/backup.txt
```
Example Output
![Backup](docs/screenshots/backup.png)

- **restore**, takes a file path argument and restores the data in the file into current data store
```
restore -f /app/restore.txt
```
Example Output
![Restore](docs/screenshots/restore.png)
